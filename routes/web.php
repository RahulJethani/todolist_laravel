<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/logout', function(){
    Auth::logout();
    return redirect('/');
});

Route::middleware(['auth'])->group(function(){
    Route::resource('/tasks', 'App\Http\Controllers\TasksController');
    Route::put('/tasks/{task}/complete', 'App\Http\Controllers\TasksController@complete')->name('tasks.complete');
    Route::get('/completedTasks', 'App\Http\Controllers\TasksController@completedTasks')->name('tasks.completedTasks');
});
