@extends('layouts.app')
@section('content')
<div class="row d-flex justify-content-center container">
    <div class="col-md-12">
        <div class="card-hover-shadow-2x mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="fa fa-tasks"></i>&nbsp;Task Lists</div>
            </div>
            <div class="scroll-area-sm">
                <perfect-scrollbar class="ps-show-limits">
                    <div style="position: static;" class="ps ps--active-y">
                        <div class="ps-content">
                            <ul class=" list-group list-group-flush">
                            @foreach ($tasks as $task)
                                <li class="list-group-item">
                                    <div class="todo-indicator bg-primary"></div>
                                    <div class="ml-2 widget-content p-0">
                                        <div class="widget-content-wrapper row">
                                            
                                            <div class="widget-content-left col-md-8">
                                                <div class="widget-heading text-left">{{ $task->task }}
                                                    <div class="badge badge-danger ml-2">{{ $task->is_deadline_expired }}</div>    
                                                </div>
                                                <div class="widget-subheading text-left text-danger">
                                                <label for="deadline">Deadline:</label>
                                                <i>{{ $task->deadline_date }}</i>
                                                </div>
                                                
                                            </div>
                                            <div class="widget-content-right text-right 
                                            col-md-4">
                                            <div class="row ml-5">
                                            <form action="{{ route('tasks.complete', $task) }}" method="POST" class="ml-5">
                                            @csrf
                                            @method('PUT')
                                             <button type="submit" class="border-0 btn-transition btn btn-outline-success"> <i class="fa fa-check"></i></button>
                                            </form>
                                              <button class="border-0 btn-transition btn btn-outline-danger" onclick= "displayModalForm({{ $task }})"  data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-trash"></i> </button>
                                            <button class="border-0 btn-transition btn btn-outline-primary" onclick= "displayEditModalForm({{ $task }})"  data-toggle="modal" data-target="#editModal"><i class="fa fa-edit"></i></button>
                                            </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </perfect-scrollbar>
            </div>
            <div class="d-block text-right card-footer"><button class="btn btn-primary" data-toggle="modal" data-target="#createModal">Add Task</button></div>
        </div>
    </div>
</div>

<!-- Create Modal -->

<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Add Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div> 
                <div class="modal-body">
                <form action="{{ route('tasks.store') }}" method="POST">
                @csrf
                    <div class="form-group">
                        <label for="task">Task:</label>
                        <input type="text" name="task" placeholder="Enter Task" value = "{{ old('task') }}" class="form-control {{ $errors->has('task') ? 'is-invalid' : '' }}">
                        @error('task')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="deadline">Deadline:</label>
                        <input id="deadline" type="text" name="deadline" value="{{ old('deadline') }}" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">         
                        @error('deadline')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-outline-success">Add Task!</button>

                    @foreach($errors as $error)
    echo($error);
@endforeach
                </form>
                </div>
            </div>
        </div>
    </div>
<!-- /Create Modal -->

<!-- Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Edit Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div> 
                <div class="modal-body"> 
                <form action="" method="POST" id="editForm">
                @csrf
                @method('PUT')
                    <div class="form-group">
                        <label for="task">Task:</label>
                        <input type="text" name="task" placeholder="Enter Task"
                        class="form-control {{ $errors->has('task') ? 'is-invalid' : '' }}">
                        @error('task')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="deadline">Deadline:</label>
                        <input id="deadline" type="text" name="deadline" value="{{ old('deadline') }}" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">         
                        @error('deadline')
                            <div class="text-danger">{{ $message }}</div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-outline-success">Edit Task!</button>

                </form>
                </div>
            </div>
        </div>
    </div>
<!-- /Edit Modal -->

<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div> 
                <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')               
                <div class="modal-body">                    
                    <p>
                        Are you sure you want to Delete Task?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete Task</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<!-- /Delete Modal -->
@endsection


@section('content')
<div class="card">
            <div class="card-header">Add Task</div>
            <div class="card-body">
                
            </div>
        </div>

@endsection



@section('page-level-scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
    flatpickr("#deadline", {
            enableTime: true
        });
    </script>
    <script type="text/javascript">
        function displayModalForm($task){
            var url = '/tasks/' + $task.id;
            $("#deleteForm").attr('action', url);
        }

        function displayEditModalForm($task){
            var url = '/tasks/' + $task.id;
            $("#editForm").attr('action', url);

            $('#editForm input[name="task"]').val($task.task);
            $('#editForm input[name="deadline"]').val($task.deadline);
        }
    </script>
@endsection

@section('page-level-styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/trix/1.2.1/trix.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link rel="stylesheet" href="{{  asset('css/app.css') }}">
 
@endsection