@extends('layouts.app')
@section('content')
<div class="row d-flex justify-content-center container">
    <div class="col-md-12">
        <div class="card-hover-shadow-2x mb-3 card">
            <div class="card-header-tab card-header">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal"><i class="fa fa-tasks"></i>&nbsp;Completed Task Lists</div>
            </div>
            <div class="scroll-area-sm">
                <perfect-scrollbar class="ps-show-limits">
                    <div style="position: static;" class="ps ps--active-y">
                        <div class="ps-content">
                            <ul class=" list-group list-group-flush">
                            @foreach ($tasks as $task)
                                <li class="list-group-item">
                                    <div class="todo-indicator bg-primary"></div>
                                    <div class="ml-2 widget-content p-0">
                                        <div class="widget-content-wrapper row">
                                            
                                            <div class="widget-content-left col-md-10">
                                                <div class="widget-heading text-left">{{ $task->task }}
                                                    @if ($task->is_deadline_expired)
                                                    <div class="badge badge-warning ml-2 p-1">
                                                    completed late
                                                    @else
                                                    <div class="badge badge-success ml-2 p-1">
                                                    completed on time   
                                                    @endif
                                                    </div>    
                                                </div>
                                                <div class="widget-subheading text-left text-danger">
                                                <label for="deadline">Completed On:</label>
                                                <i>{{ $task->completed_on_date }}</i>
                                                </div>
                                                
                                            </div>
                                            <div class="widget-content-right text-right col-md-2">  <button class="border-0 btn-transition btn btn-outline-danger" onclick= "displayModalForm({{ $task }})"  data-toggle="modal" data-target="#deleteModal"> <i class="fa fa-trash"></i> </button>
                                                 </div>
                                            
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </perfect-scrollbar>
            </div>
        </div>
    </div>
</div>


<!-- Delete Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="modelTitleId" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Delete Task</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                </div> 
                <form action="" method="POST" id="deleteForm">
                @csrf
                @method('DELETE')               
                <div class="modal-body">                    
                    <p>
                        Are you sure you want to Delete Task?
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger">Delete Task</button>
                </div>
                </form>
            </div>
        </div>
    </div>
<!-- /Delete Modal -->

@endsection

@section('page-level-scripts')
    <script type="text/javascript">
        function displayModalForm($task){
            var url = '/tasks/' + $task.id;
            $("#deleteForm").attr('action', url);
        }
    </script>
@endsection