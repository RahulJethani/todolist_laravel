<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'task',
        'status',
        'deadline',
        'completed_on'
    ];

    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getDeadlineDateAttribute(){
        $data = $this->deadline;
        $newData = strtotime($data);
        $date = date('d M, Y | h:i A', $newData);
        return $date;
    }

    public function getCompletedOnDateAttribute(){
        $data = $this->completed_on;
        $newData = strtotime($data);
        $date = date('d M, Y | h:i A', $newData);
        return $date;
    }

    public function getisDeadlineExpiredAttribute(){
        if(date(Carbon::now()) > date(Carbon::parse($this->deadline))){
            return "Expired";
        }
        else{
            return "";
        }    
    }
}

