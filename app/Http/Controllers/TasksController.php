<?php

namespace App\Http\Controllers;

use App\Http\Requests\tasks\CreateTaskRequest;
use App\Http\Requests\tasks\UpdateTaskRequest;
use App\Models\Task;
use Carbon\Carbon;
use DateTime;
use Facade\Ignition\Tabs\Tab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // pending tasks default krna baaki hai!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        $tasks = Task::all()->where('user_id', '=', Auth::id())->where('status', '=', 'pending');
        return view('tasks.index', compact([
            'tasks'
        ]));
    }

    public function completedTasks(){
        $tasks = Task::all()->where('status', '=', 'completed')->where('user_id', '=', Auth::id());
        return view('tasks.completed', compact([
            'tasks'
        ]));
    }

    public function complete(Task $task){
        $currentDateTime = Carbon::now()->toDateTimeString();

        $task->update([
            'status' => 'completed',
            'completed_on' => $currentDateTime
        ]);
        session()->flash('success', 'Task Completed!');
        return redirect(route('tasks.completedTasks'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateTaskRequest $request)
    {
        try {
            $task = auth()->user()->tasks()->create([
                'task' => $request->task,
                'deadline' => $request->deadline
            ]);

    
        } catch (\Throwable $th) {
            dd($th);
        }
        
        session()->flash('success', 'Task Created!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function show(Task $task)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function edit(Task $task)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTaskRequest $request, Task $task)
    {
        $data = request()->only([
            'task',
            'deadline'
        ]);

        $task->update($data);

        session()->flash('success', 'Task Updated!');
        return redirect(route('tasks.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Task  $task
     * @return \Illuminate\Http\Response
     */

    public function trash(Task $task){
        
    }
    public function destroy(Task $task)
    {
        $task->forceDelete();
        session()->flash('success', "Task Deleted Successfully!");
        return redirect()->back();
    }
}
