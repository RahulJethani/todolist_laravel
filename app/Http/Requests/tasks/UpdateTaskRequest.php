<?php

namespace App\Http\Requests\tasks;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [       
            'task'=>'required|unique:tasks,task,'.$this->task,
            'deadline' => 'date|after:now',
            'status' => 'string',
            'user_id' => 'exists:users,id',
            'completed_on' => 'date'
        ];
    }
}
